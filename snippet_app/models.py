from django.db import models

# Create your models here.
class Category(models.Model):
	name = models.CharField(max_length=20)
	slug = models.SlugField()
	logo = models.ImageField(upload_to = 'photos/%Y/%m/%d', blank = False)
	description = models.TextField(max_length=200)

	# class Meta:
	# 	db_table = 'category'

	def __str__(self):
		return self.name


class Collection(models.Model):
	title = models.CharField(max_length=20)
	category_name = models.ForeignKey(Category, on_delete = models.CASCADE) 
	description = models.TextField(max_length=200)



	def __str__(self):
		return self.title