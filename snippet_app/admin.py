from django.contrib import admin

# Register your models here.
from .models import Category, Collection


admin.site.register(Category)
admin.site.register(Collection)