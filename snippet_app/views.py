from django.shortcuts import render
from django.views.generic import ListView
from .models import Category, Collection

# Create your views here.

class HomePageView(ListView):
	template_name 		= 'snippet_app/home.html'
	model		 		= 'Category'	
	context_object_name = 'categories' 

	def get_queryset(self):
		return Category.objects.all()

class AboutView(ListView):
	template_name 		= 'snippet_app/about.html'
	model 		  		= 'Collection'
	context_object_name = 'collections'


	def get_queryset(self):
		return Collection.objects.all()
