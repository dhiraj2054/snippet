from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from snippet_app.models import Category, Collection
from category.models import Contact
# from .models import Profile





#signupform
class SignupForm(UserCreationForm):
	# first_name = forms.CharField(max_length=30, required=True, help_text='Mandatory.')
	# last_name = forms.CharField(max_length=30, required=True, help_text='Mandatory.')
	# email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
	
	
	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2' )


#categoryForm
class CategoryForm(forms.ModelForm):
	class Meta:
		model = Category
		fields = ['name', 'slug', 'logo', 'description']

#collectionForm
class CollectionForm(forms.ModelForm):
	class Meta:
		model = Collection
		fields = ['title', 'category_name', 'description']


#ContactForm
class ContactForm(forms.Form):
    first_name = forms.CharField(max_length=400)
    last_name = forms.CharField(max_length=400)
    email = forms.EmailField(required=True)
    subject = forms.CharField(required=True)
    message = forms.CharField(widget=forms.Textarea, required=True)

    # phone = forms.IntegerField()
    # address = forms.CharField(max_length=50)
   
    class Meta:
    	model = Contact
    

    def send_email(self):
        pass
        

 
class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']