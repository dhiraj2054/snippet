from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView, FormView
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse, request


from django.contrib.auth import login, authenticate


from snippet_app.models import Category, Collection
from .forms import CategoryForm, CollectionForm, SignupForm, ContactForm, UserForm
from .models import Profile
from django.contrib.auth.models import User

from django.contrib.auth.decorators import login_required
from django.forms.models import inlineformset_factory
from django.core.exceptions import PermissionDenied


#CategoryCreateView
class CategoryCreateView(CreateView):
	template_name = 'category/catcreate.html'
	model = Category
	form_class = CategoryForm
	success_url= reverse_lazy('category_list')

	def get_context_data(self,*args,**kwargs):
		context = super().get_context_data(*args,**kwargs)
		print(context)
		return context

#CategoryListView
class CategoryListView(ListView):
	template_name = 'category/catList.html'
	context_object_name = 'categories'

	def get_queryset(self):
		return Category.objects.all() 

#CategoryDetailView
class CategoryDetailView(DetailView):
	model = Category
	template_name = 'category/category.html'

#CategoryUpdateView
class CategoryUpdateView(UpdateView):
	template_name = 'category/catupdate.html'
	model = Category
	fields = ['name', 'slug', 'logo', 'description']
	success_url = reverse_lazy('category_list')


	# form_class = CategoryForm
	# success_url = reverse_lazy('category/category_list')

	# def get_object(self):
	# 	id = self.kwargs.get("pk")
	# 	return get_object_or_404(Category, id=id)

	# def form_valid(self,form):
	# 	print(form.cleaned_data)
	# 	return super().form_valid(form)



#CategoryDeleteView
class CategoryDeleteView(DeleteView):
	template_name = 'category/catdelete.html'
	model = Category
	success_url = reverse_lazy('category_list')



  


###########################################################################################################
#CollectionCreateView
class CollectionCreateView(CreateView):
	template_name = 'category/collcreate.html'
	model = Collection
	form_class = CollectionForm
	success_url = reverse_lazy('collection_list')

	
	def get_context_data(self, *args, **kwargs):
		c = super().get_context_data(*args, **kwargs)
		print(c)
		return c


#CollectionList
class CollectionListView(ListView):
	template_name = 'category/collList.html'
	context_object_name = 'collections'

	def get_queryset(self):
		return Collection.objects.all() 

#CollectionDetail
class CollectionDetailView(DetailView):
	model = Collection
	template_name = 'category/collection.html'    

	def get_queryset(self):
		return Collection.objects.all() 

#CollectionUpdateView
class CollectionUpdateView(UpdateView):
	template_name = 'category/collupdate.html'
	model = Collection
	fields = ['title', 'category_name', 'description']
	success_url = reverse_lazy('collection_list')


#CollectionDeleteView
class CollectionDeleteView(DeleteView):
	template_name = 'category/colldelete.html'
	model = Collection
	success_url = reverse_lazy('collection_list')

	

#########################################################################################################                

#SignupView
class SignupView(FormView):
	template_name = 'category/signup.html'
	form_class = SignupForm
	

	def form_valid(self, form):
		form.save()
		username = form.cleaned_data.get('username')
		raw_password = form.cleaned_data.get('password1')
		user = authenticate(username=username, password=raw_password)
		login(self.request, user)
		return redirect('accountcreated')


def accountcreatedView(request):
	return HttpResponse('Success! Your account has been created.')



#ContactView
class ContactView(FormView):
	template_name = 'category/contact.html'
	form_class = ContactForm
	success_url = 'success'

	def form_valid(self, form):
		form.send_email()
		return super(ContactView, self).form_valid(form)


###############################################################################################################

#UserProfileEdit (https://blog.khophi.co/extending-django-user-model-userprofile-like-a-pro/)

@login_required() # only logged in users should access this
def edit_user(request):
	pk = request.user.pk 
	user = User.objects.get(pk=pk)

	# prepopulate UserProfileForm with retrieved user values from above.
	user_form = UserForm(instance=user)

	ProfileInlineFormset = inlineformset_factory(User, Profile, fields=('profile_pic','website', 'bio', 'phone', 'city', 'country', 'organization'))
	formset = ProfileInlineFormset(instance=user)
 
	if request.user.is_authenticated() and request.user.id == user.id:
		if request.method == "POST":
			user_form = UserForm(request.POST, request.FILES, instance=user)
			formset = ProfileInlineFormset(request.POST, request.FILES, instance=user)

			if user_form.is_valid():
				created_user = user_form.save(commit=False)
				formset = ProfileInlineFormset(request.POST, request.FILES, instance=created_user)

				if formset.is_valid():
					created_user.save()
					formset.save()
					return HttpResponseRedirect('/')
 

		return render(request, "category/user_update.html", {
			"noodle": pk,
			"noodle_form": user_form,
			"formset": formset,            

			})

	else:
		raise PermissionDenied 
		   
	 



# class UpdateProfile(UpdateView):
#     model = User
#     fields = ['first_name', 'last_name'] 
#     template_name = 'category/user_update.html'
	
#     def get_object(self, queryset=None):
#         return self.request.user


#Success View
def successView(request):
    return HttpResponse ('Success! Thank you for your message.')

