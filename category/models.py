from django.db import models
from django.contrib.auth.models import User
#from django.urls import reverse
from django.db.models.signals import post_save

# Create your models here.

class Contact(models.Model):
	First_Name = models.CharField(max_length=50)
	Last_Name = models.CharField(max_length=50)
	Email = models.EmailField()
	Subject = models.CharField(max_length=100)
	Message = models.TextField(max_length=200)
	# phone = models.IntegerField()
	# address = models.CharField(max_length=50)
	


	class Meta:
	   db_table = 'Contact'


	def __str__(self):
		return self.name


#https://django.readthedocs.io/en/2.1.x/topics/auth/customizing.html#extending-the-existing-user-model
class Profile(models.Model):
	user = models.OneToOneField(User, on_delete = models.CASCADE, related_name='profile')
	profile_pic = models.ImageField(upload_to='profile_pic', blank=True)
	bio = models.TextField(max_length=1000)
	website = models.URLField(default='', blank=True)
	phone = models.CharField(max_length=20, blank=True, default='')
	city = models.CharField(max_length=100, default='', blank=True)
	country = models.CharField(max_length=100, default='', blank=True)
	organization = models.CharField(max_length=100, default='', blank=True)



	def __str__(self):
		return "{0} {1}".format(self.user.first_name, self.user.last_name)



def create_profile(sender, **kwargs):
	user = kwargs["instance"]
	if kwargs["created"]:
		user_profile = Profile(user=user)
		user_profile.save()
post_save.connect(create_profile, sender=User)