from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.http import request
from . import views

from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [

	#CATEGORY

	url(r'^$', views.CategoryListView.as_view(), name = 'category_list'),
	url(r'^create/', views.CategoryCreateView.as_view(), name= 'category_create'),
	url(r'^(?P<pk>\d+)/$', views.CategoryDetailView.as_view(), name = 'category_detail'),
	url(r'^(?P<pk>\d+)/update', views.CategoryUpdateView.as_view(), name = 'category_update'),
	url(r'^(?P<pk>\d+)/delete', views.CategoryDeleteView.as_view(), name = 'category_delete'),

	#COLLECTION
	url(r'^collection/create/', views.CollectionCreateView.as_view(), name = 'collection_create'),
	url(r'^collection/$', views.CollectionListView.as_view(), name = 'collection_list'),
	url(r'^collection/(?P<pk>\d+)/$', views.CollectionDetailView.as_view(), name = 'collection_detail'),
	url(r'^collection/(?P<pk>\d+)/update/$', views.CollectionUpdateView.as_view(), name = 'collection_update'),
	url(r'^collection/(?P<pk>\d+)/delete', views.CollectionDeleteView.as_view(), name = 'collection_delete'),

	#SIGNUP
	url(r'^signup/$', views.SignupView.as_view(), name = 'signup'),
	url(r'^signup/accountcreated/$', views.accountcreatedView, name = 'accountcreated'),
	url(r'^login/', auth_views.login, {'template_name' : 'category/login.html'}, name = 'login'),
	url(r'^logout/', auth_views.logout, {'next_page' : 'index'}, name = 'logout'),

	#PASSWORD_RESET
	url(r'^password_reset/$', auth_views.password_reset, name = 'password_reset'),
	url(r'^password_reset/done/$', auth_views.password_reset_done, name = 'password_reset_done'),
	url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.password_reset_confirm, name='password_reset_confirm'),
	url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),


	#Contact
	url(r'^contact/$', views.ContactView.as_view(), name = 'contact'),
	url(r'^contact/success', views.successView, name = 'success'),

	url(r'^update/$', views.edit_user, name = 'edit_user'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)